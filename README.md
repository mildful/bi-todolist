# behaviorinteractive

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Notes
Étant donné la simplicité du projet, j'ai décidé de ne pas utiliser de store, de router, de typescript, de i18n, etc.

